//
//  CalendarHelperTests.swift
//  CalendarTests
//
//  Created by Evan Heintschel on 3/29/22.
//

import XCTest
@testable import Calendar

class CalendarHelperTests: XCTestCase {
    
    let calendarHelper = CalendarHelper()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func getDate(from dateStr: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale(identifier: "en_US_POSIX")
        guard let date = formatter.date(from: dateStr) else { fatalError("Could not buid from dateStr: \(dateStr)") }
        return date
    }
    
    //MARK: - String Formatting tests
    
    func test_yearString() throws {
        let year = "2022"
        let date = getDate(from: "\(year)/03/31 12:00")
        XCTAssertEqual(calendarHelper.yearString(date: date), year)
    }
    
    func test_monthString() throws {
        typealias Month = (num: String, str: String)
        let months: [Month] = [(num: "01", str: "January"), (num: "02", str: "February"), (num: "03", str: "March"), (num: "04", str: "April"), (num: "05", str: "May"), (num: "06", str: "June"), (num: "07", str: "July"), (num: "08", str: "August"), (num: "09", str: "September"), (num: "10", str: "October"), (num: "11", str: "November"), (num: "12", str: "December")]
        
        for month in months {
            let date = getDate(from: "2022/\(month.num)/20 12:00")
            XCTAssertEqual(calendarHelper.monthString(date: date), month.str)
        }
    }
    
    func test_monthDayString() throws {
        typealias Month = (num: String, str: String)
        let months: [Month] = [(num: "01", str: "January"), (num: "02", str: "February"), (num: "03", str: "March"), (num: "04", str: "April"), (num: "05", str: "May"), (num: "06", str: "June"), (num: "07", str: "July"), (num: "08", str: "August"), (num: "09", str: "September"), (num: "10", str: "October"), (num: "11", str: "November"), (num: "12", str: "December")]
        let day = "20"
        
        for month in months {
            let date = getDate(from: "2022/\(month.num)/\(day) 12:00")
            let expectedMonthDay = "\(month.str) \(day)"
            XCTAssertEqual(calendarHelper.monthDayString(date: date), expectedMonthDay)
        }
    }
    
    func test_timeString() throws {
        let time = "12:00"
        let date = getDate(from: "2022/03/31 \(time)")
        XCTAssertEqual(calendarHelper.timeString(date: date), time)
    }
    
    
    // MARK: - Month Info/Calculation tests

    func test_daysInMonth() throws {
        typealias MonthDays = (month: String, days: Int)
        let mds: [MonthDays] = [(month: "01", days: 31), (month: "02", days: 28), (month: "03", days: 31), (month: "04", days: 30), (month: "05", days: 31), (month: "06", days: 30), (month: "07", days: 31), (month: "08", days: 31), (month: "09", days: 30), (month: "10", days: 31), (month: "11", days: 30), (month: "12", days: 31)]
        let leapYear = getDate(from: "2020/02/29 12:00")
        
        for md in mds {
            let date = getDate(from: "2022/\(md.month)/20 12:00")
            XCTAssertEqual(calendarHelper.daysInMonth(date: date), md.days)
        }
        XCTAssertEqual(calendarHelper.daysInMonth(date: leapYear), 29)
    }
    
    func test_dayOfMonth() throws {
        for day in 1...31 {
            let date = getDate(from: "2022/03/\(day) 12:00")
            XCTAssertEqual(calendarHelper.dayOfMonth(date: date), day)
        }
    }
    
    func test_plusMonth() throws {
        let HHmm = "22:31"
        let date = getDate(from: "2022/03/31 \(HHmm)")
        let expectedDate = getDate(from: "2022/04/30 \(HHmm)")
        let date2 = getDate(from: "2022/04/30 \(HHmm)")
        let expectedDate2 = getDate(from: "2022/05/30 \(HHmm)")
        let date3 = getDate(from: "2022/01/30 \(HHmm)")
        let expectedDate3 = getDate(from: "2022/02/28 \(HHmm)")
        let date4 = getDate(from: "2024/01/30 \(HHmm)")
        let expectedDate4 = getDate(from: "2024/02/29 \(HHmm)")
        let date5 = getDate(from: "2024/02/28 \(HHmm)")
        let expectedDate5 = getDate(from: "2024/03/28 \(HHmm)")
        XCTAssertEqual(calendarHelper.plusMonth(date: date), expectedDate)
        XCTAssertEqual(calendarHelper.plusMonth(date: date2), expectedDate2)
        XCTAssertEqual(calendarHelper.plusMonth(date: date3), expectedDate3)
        XCTAssertEqual(calendarHelper.plusMonth(date: date4), expectedDate4)
        XCTAssertEqual(calendarHelper.plusMonth(date: date5), expectedDate5)
    }
    
    func test_minusMonth() throws {
        let HHmm = "22:31"
        let date = getDate(from: "2022/04/30 \(HHmm)")
        let expectedDate = getDate(from: "2022/03/30 \(HHmm)")
        let date2 = getDate(from: "2022/05/31 \(HHmm)")
        let expectedDate2 = getDate(from: "2022/04/30 \(HHmm)")
        let date3 = getDate(from: "2022/03/30 \(HHmm)")
        let expectedDate3 = getDate(from: "2022/02/28 \(HHmm)")
        let date4 = getDate(from: "2024/03/30 \(HHmm)")
        let expectedDate4 = getDate(from: "2024/02/29 \(HHmm)")
        XCTAssertEqual(calendarHelper.minusMonth(date: date), expectedDate)
        XCTAssertEqual(calendarHelper.minusMonth(date: date2), expectedDate2)
        XCTAssertEqual(calendarHelper.minusMonth(date: date3), expectedDate3)
        XCTAssertEqual(calendarHelper.minusMonth(date: date4), expectedDate4)
    }
    
    func test_firstOfMonth() throws {
        let months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
        for month in months {
            let date = getDate(from: "2022/\(month)/20 00:00")
            let expected = getDate(from: "2022/\(month)/01 00:00")
            XCTAssertEqual(calendarHelper.firstOfMonth(date: date), expected)
        }
    }
    
    
    //MARK: - Week Info/Calculation tests
    
    func test_weekDay() throws {
        typealias DateDays = (dateStr: String, day: Int)
         let dateDays = [(dateStr: "2022/03/28 12:00", day: 1), (dateStr: "2022/03/29 12:00", day: 2), (dateStr: "2022/03/30 12:00", day: 3), (dateStr: "2022/03/31 12:00", day: 4), (dateStr: "2022/04/01 12:00", day: 5), (dateStr: "2022/04/02 12:00", day: 6), (dateStr: "2022/04/03 12:00", day: 0)]
        for dd in dateDays {
            let date = getDate(from: dd.dateStr)
            XCTAssertEqual(calendarHelper.weekDay(date: date), dd.day)
        }
    }
    
    func test_weekDayAsString() throws {
        typealias DateDays = (dateStr: String, day: Day)
        let dateDays = [(dateStr: "2022/03/28 12:00", day: Day.monday), (dateStr: "2022/03/29 12:00", day: Day.tuesday), (dateStr: "2022/03/30 12:00", day: Day.wednesday), (dateStr: "2022/03/31 12:00", day: Day.thursday), (dateStr: "2022/04/01 12:00", day: Day.friday), (dateStr: "2022/04/02 12:00", day: Day.saturday), (dateStr: "2022/04/03 12:00", day: Day.sunday)]
        for dd in dateDays {
            let date = getDate(from: dd.dateStr)
            XCTAssertEqual(calendarHelper.weekDayAsString(date: date), dd.day.rawValue)
        }
    }
    
    func test_sundayForDay() throws {
        let expectedDate = getDate(from: "2022/03/27 12:00")
        let weekOfDateStrings = ["2022/03/27 12:00", "2022/03/28 12:00", "2022/03/29 12:00", "2022/03/30 12:00", "2022/03/31 12:00", "2022/04/01 12:00", "2022/04/02 12:00"]
        for dateStr in weekOfDateStrings {
            let date = getDate(from: dateStr)
            XCTAssertEqual(calendarHelper.sundayForDay(date: date), expectedDate)
        }
    }
    
    
    //MARK: - Day Info/Calculation tests
    
    func test_hourFromDay() throws {
        typealias Hour = (str: String, int: Int)
        let hours = [(str: "00", int: 0), (str: "01", int: 1), (str: "02", int: 2), (str: "03", int: 3), (str: "04", int: 4), (str: "05", int: 5), (str: "06", int: 6), (str: "07", int: 7), (str: "08", int: 8), (str: "09", int: 9), (str: "10", int: 10), (str: "11", int: 11), (str: "12", int: 12), (str: "13", int: 13), (str: "14", int: 14), (str: "15", int: 15), (str: "16", int: 16), (str: "17", int: 17), (str: "18", int: 18), (str: "19", int: 19), (str: "20", int: 20), (str: "21", int: 21), (str: "22", int: 22), (str: "23", int: 23)]
        for hour in hours {
            let date = getDate(from: "2022/04/01 \(hour.str):00")
            XCTAssertEqual(calendarHelper.hourFromDay(date: date), hour.int)
        }
    }
    
    func test_addDays() throws {
        let baseDay = 22
        let baseDate = getDate(from: "2022/03/\(baseDay) 12:00")
        let toAddBy = [1, 7, -1, -7]
        for days in toAddBy {
            let expectedDay = baseDay + days
            let expectedDate = getDate(from: "2022/03/\(expectedDay) 12:00")
            XCTAssertEqual(calendarHelper.addDays(date: baseDate, days: days), expectedDate)
        }
    }
}
