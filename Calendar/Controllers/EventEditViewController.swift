//
//  EventEditViewController.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/24/22.
//

import UIKit

class EventEditViewController: UIViewController  {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.date = selectedDate
    }
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        let newEvent = Event()
        newEvent.id = eventList.count
        newEvent.name = nameTextField.text
        newEvent.date = datePicker.date
        
        eventList.append(newEvent)
        navigationController?.popViewController(animated: true)
    }
}
