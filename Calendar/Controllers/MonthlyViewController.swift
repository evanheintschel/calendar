//
//  MonthlyViewController.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/23/22.
//

import UIKit
 
class MonthlyViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var totalSquares = [CalendarDay]()
    let calendarHelper = CalendarHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCellsView()
        setMonthView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setMonthView()
    }
    
    func setCellsView() {
        let width = (collectionView.frame.size.width - 2) / 8
        let height = (collectionView.frame.size.height - 2) / 8
        
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: width, height: height)
    }

    func setMonthView() {
        totalSquares.removeAll()
        let daysInMonth = calendarHelper.daysInMonth(date: selectedDate)
        let firstDayOfMonth = calendarHelper.firstOfMonth(date: selectedDate)
        let startingSpaces = calendarHelper.weekDay(date: firstDayOfMonth)
        let previousMonth = calendarHelper.minusMonth(date: selectedDate)
        let daysInPreviousMonth = calendarHelper.daysInMonth(date: previousMonth)
        var count: Int = 1
        
        while (count <= 42) {
            let calendarDay = CalendarDay()
            if (count <= startingSpaces) {
                let previousMonthDay = daysInPreviousMonth - startingSpaces + count
                calendarDay.day = String(previousMonthDay)
                calendarDay.month = CalendarDay.Month.previous
            }
            else if (count - startingSpaces > daysInMonth) {
                calendarDay.day = String(count - daysInMonth - startingSpaces)
                calendarDay.month = CalendarDay.Month.next
            } else {
                calendarDay.day = String(count - startingSpaces)
                calendarDay.month = CalendarDay.Month.current
            }
            totalSquares.append(calendarDay)
            count += 1
        }
        monthLabel.text = calendarHelper.monthString(date: selectedDate) + " " + calendarHelper.yearString(date: selectedDate)
        collectionView.reloadData()
    }
    
    @IBAction func previousMonth(_ sender: UIButton) {
        selectedDate = calendarHelper.minusMonth(date: selectedDate)
        setMonthView()
    }
    
    @IBAction func nextMonth(_ sender: UIButton) {
        selectedDate = calendarHelper.plusMonth(date: selectedDate)
        setMonthView()
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
}

extension MonthlyViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalSquares.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calCell", for: indexPath) as! CalendarCell
        let calendarDay = totalSquares[indexPath.item]
        cell.dayOfMonth.text = calendarDay.day
        if calendarDay.month == CalendarDay.Month.current {
            cell.dayOfMonth.textColor = UIColor.black
        } else {
            cell.dayOfMonth.textColor = UIColor.gray
        }
        return cell
    }
}
