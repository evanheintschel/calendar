//
//  CalendarHelper.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/23/22.
//

import Foundation

class CalendarHelper {
    let calendar = Calendar.current
    
    //MARK: - String Formatting functions
    
    func yearString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: date)
    }
    
    func monthString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: date)
    }
    
    func monthDayString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL dd"
        return dateFormatter.string(from: date)
    }
    
    func timeString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    
    // MARK: - Month Info/Calculation functions
    
    func daysInMonth(date: Date) -> Int {
        guard let range = calendar.range(of: .day, in: .month, for: date) else {
            fatalError("Couldn't calculate range given \(date)")
        }
        return range.count
    }
    
    func dayOfMonth(date: Date) -> Int {
        let components = calendar.dateComponents([.day], from: date)
        guard let day = components.day else {
            fatalError("Couldn't calculate dayOfMonth given \(date)")
        }
        return day
    }
    
    func plusMonth(date: Date) -> Date {
        guard let plusMonth = calendar.date(byAdding: .month, value: 1, to: date) else {
            fatalError("Couldn't calculate plusMonth given \(date)")
        }
        return plusMonth
    }
    
    func minusMonth(date: Date) -> Date {
        guard let minusMonth = calendar.date(byAdding: .month, value: -1, to: date) else {
            fatalError("Couldn't calculate minusMonth given \(date)")
        }
        return minusMonth
    }
    
    func firstOfMonth(date: Date) -> Date {
        let components = calendar.dateComponents([.year, .month], from: date)
        guard let firstOfMonth = calendar.date(from: components) else {
            fatalError("Couldn't calculate firstOfMonth given \(date)")
        }
        return firstOfMonth
    }
    
    
    //MARK: - Week Info/Calculation functions
    
    func weekDay(date: Date) -> Int {
        let components = calendar.dateComponents([.weekday], from: date)
        guard let weekday = components.weekday else {
            fatalError("Couldn't calculate weekDay given \(date)")
        }
        return weekday - 1
    }
    
    func weekDayAsString(date: Date) -> String {
        switch weekDay(date: date) {
        case 0:
            return Day.sunday.rawValue
        case 1:
            return Day.monday.rawValue
        case 2:
            return Day.tuesday.rawValue
        case 3:
            return Day.wednesday.rawValue
        case 4:
            return Day.thursday.rawValue
        case 5:
            return Day.friday.rawValue
        case 6:
            return Day.saturday.rawValue
        default:
            fatalError("Couldn't calculate weekDay as String given date \(date)")
        }
    }
    
    func sundayForDay(date: Date) -> Date {
        let today = weekDay(date: date)
        if today == 0 {
            // today is Sunday, return
            return date
        }
        // subtract however many days today is from Sunday
        return addDays(date: date, days: -1 * today)
    }
    
    
    //MARK: - Day Info/Calculation functions
    
    func hourFromDay (date: Date) -> Int {
        let components = calendar.dateComponents([.hour], from: date)
        guard let day = components.hour else {
            fatalError("Couldn't calculate dayOfMonth given \(date)")
        }
        return day
    }
    
    func addDays(date: Date, days: Int) -> Date {
        guard let dateWithAddedDays = calendar.date(byAdding: .day, value: days, to: date) else {
            fatalError("Couldn't add \(days) days to date \(date)")
        }
        return dateWithAddedDays
    }
}
