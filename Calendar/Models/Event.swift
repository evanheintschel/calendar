//
//  Event.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/24/22.
//

import Foundation

var eventList = [Event]()

class Event {
    var id: Int!
    var name: String!
    var date: Date!
    
    func eventsForDate(date: Date) -> [Event] {
        var daysEvents = [Event]()
        for event in eventList {
            if Calendar.current.isDate(event.date, inSameDayAs: date) {
                daysEvents.append(event)
            }
        }
        return daysEvents
    }
    
    func eventsForDateAndTime(date: Date, hour: Int ) -> [Event] {
        var daysEvents = [Event]()
        for event in eventList {
            if Calendar.current.isDate(event.date, inSameDayAs: date) {
                let eventHour = CalendarHelper().hourFromDay(date: event.date)
                if eventHour == hour {
                    daysEvents.append(event)
                }
            }
        }
        return daysEvents
    }
}
