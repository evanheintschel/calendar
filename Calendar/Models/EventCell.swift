//
//  EventCell.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/24/22.
//

import UIKit

class EventCell: UITableViewCell {
    @IBOutlet weak var eventLabel: UILabel!
}
