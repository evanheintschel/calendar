//
//  DailyCell.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/26/22.
//

import UIKit

class DailyCell: UITableViewCell {
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var event1: UILabel!
    @IBOutlet weak var event2: UILabel!
    @IBOutlet weak var event3: UILabel!
}
