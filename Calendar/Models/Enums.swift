//
//  Enums.swift
//  Calendar
//
//  Created by Evan Heintschel on 5/25/22.
//

import Foundation

enum Day: String {
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
    case sunday = "Sunday"
}
