//
//  CalendarDay.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/26/22.
//

import Foundation

class CalendarDay {
    var day: String!
    var month: Month!
    
    enum Month {
        case previous
        case current
        case next
    }
}
