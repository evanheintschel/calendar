//
//  CalendarCell.swift
//  Calendar
//
//  Created by Evan Heintschel on 3/23/22.
//

import UIKit

class CalendarCell: UICollectionViewCell {
    @IBOutlet weak var dayOfMonth: UILabel!
}
